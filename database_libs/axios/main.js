module.exports = class AxiosReq {
  
    SelectAllElements ($tableName) {
        return new Promise((resolve, reject)=>{
            this.pool.query('SELECT * FROM '+$tableName+' ',  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    GetAllDrivers () {
        return new Promise((resolve, reject)=>{
            this.pool.query('SELECT * FROM users WHERE group_id = 2 ',  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getDriverData ($driverId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT * FROM users WHERE id =${$driverId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    GetAllUsers () {
        return new Promise((resolve, reject)=>{
            this.pool.query('SELECT * FROM users WHERE group_id = 1 ',  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getUserData (userId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT * FROM users WHERE id =${userId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getUserTrips (userId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT
    *
FROM
    trips As Trips
INNER JOIN users AS Users ON
    Users.id =(Trips.user_id)
INNER JOIN locations AS Locations ON
    Locations.id =(Trips.from_location_id)
WHERE Trips.user_id =${userId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getTripResult (tripId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT
    *
FROM
    trips As Trips
INNER JOIN users AS Users ON
    Users.id =(Trips.user_id)
INNER JOIN locations AS Locations ON
    Locations.id =(Trips.from_location_id)
WHERE Trips.id =${tripId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
}