module.exports = class Mysql {
    constructor(pool){
        this.pool = pool ;
      }
    SelectAllElements ($tableName) {
        return new Promise((resolve, reject)=>{
            this.pool.query('SELECT * FROM '+$tableName+' ',  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    GetAllDrivers () {
        return new Promise((resolve, reject)=>{
            this.pool.query('SELECT * FROM users WHERE group_id = 2 ',  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getDriverData ($driverId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT * FROM users WHERE id =${$driverId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    GetAllUsers () {
        return new Promise((resolve, reject)=>{
            this.pool.query('SELECT * FROM users WHERE group_id = 1 ',  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getUserData (userId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT * FROM users WHERE id =${userId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getUserTrips (userId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT
    *
FROM
    trips As Trips
INNER JOIN users AS Users ON
    Users.id =(Trips.user_id)
INNER JOIN locations AS Locations ON
    Locations.id =(Trips.from_location_id)
WHERE Trips.user_id =${userId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            });
        });
    };
    getTripResult (tripId) {
        return new Promise((resolve, reject)=>{
            this.pool.query(`SELECT
            Trips.id AS id,
            Trips.user_id AS user_id,
            Trips.driver_id AS driver_id,
            Trips.from_location_id AS from_location_id,
            Trips.to_long AS to_long,
            Trips.to_lat AS to_lat,
            Trips.to_location AS to_location,
            Trips.distance AS distance,
            Trips.duration AS duration,
            Trips.cost AS cost,
            Trips.start_time AS start_time,
            Trips.end_time AS end_time,
            Trips.wait_time AS wait_time,
            Trips.status AS status,
            Trips.trip_rate AS trip_rate,
            Trips.driver_rate AS driver_rate,
            Trips.note AS note,
            Trips.created AS created,
            Trips.modified AS modified,
            Users.id AS User_id,
            Users.first_name AS User_first_name,
            Users.last_name AS User_last_name,
            Users.image AS User_image,
            Users.email AS User_email,
            Users.phone AS User_phone,
            Users.password AS User_password,
            Users.status AS User__status,
            Users.access_token AS User_access_token,
            Users.fcm AS User_fcm,
            Users.otp AS User_otp,
            Users.group_id AS User_group_id,
            Users.remember_token AS User_remember_token,
            Users.token_verified_at AS User_token_verified_at,
            Users.created AS User_created,
            Users.modified AS User_modified,
            Locations.id AS Location_id,
            Locations.longitude AS Locations_longitude,
            Locations.latitude AS Locations_latitude,
            Locations.location AS Locations_location,
            Locations.user_id AS Locations_user_id,
            Locations.created AS Locations_created,
            Locations.modified AS Locations_modified
            FROM
            trips As Trips
        INNER JOIN users AS Users ON
            Users.id =(Trips.user_id)
        INNER JOIN locations AS Locations ON           
            Locations.id =(Trips.from_location_id)
            WHERE Trips.id =${tripId} `,  (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
            }); 
        });
    };
}