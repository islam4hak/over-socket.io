module.exports = function (app) {
    const path = require('path');
    app.get('/users', function(req, res) {
        res.sendFile(path.join(__dirname, '../public/users.html'));
    })
}