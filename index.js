const express =  require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const port = 3000;
const path = require('path');
const mysql = require('mysql');
const shared_vars = require('./socket_events/shared_vars.js');

const pool = mysql.createPool({
    connectionLimit: 10,    
    password: "Over@123456",
    user: "macbereg_over",
    database: "macbereg_over",
    host: "macber-eg.com",
    port: "3306"
});

 
function MyLog ($msg , $data = [],  $scope = " ") {
    console.log('%c ['+scope+'] ' +$msg+' ', 'background: #222; color: #bada55');
    console.log($data)
}
server.listen(process.env.PORT || 3000, function() {
    console.log('Express app listening at ' + process.env.PORT);
  });

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.broadcast.emit('hi');

    socket.on('disconnect', () => {
        console.log('a user disconnected');
    });

    socket.on('user send message', (msg) => {
        console.log('oum el username hennnnnnnnnnnnnnnnnnnnnnnna ')
        console.log(msg.message)
        console.log(msg.username)
        console.log(msg.user_id)
        console.log(msg.driver_id)
        console.log(msg)
        let userObject = shared_vars.getUserByUserId(msg.user_id);
        console.log(userObject.socket_id);
        let driverObject  = shared_vars.getDriverByUserId(msg.driver_id);
        console.log(driverObject.socket_id);
        io.to(driverObject.socket_id).emit('sent messages', {message: msg.message, sender: msg.username })
        io.to(userObject.socket_id).emit('sent messages', {message: msg.message, sender: msg.username })
    });

    // socket.on('driver send message', (msg) => {
    //     console.log('oum el driver hennnnnnnnnnnnnnnnnnnnnnnna ');
    //     console.log(msg.message)
    //     socket.emit('user recieve message', {message: msg.message, sender: "Aliiiiiii" });
    // });
});

app.use(express.static(path.join(__dirname, 'public')));

app.get('/',(req,res)=>{
    res.sendFile(__dirname + '/public/index.html');
})
require('./express_libs/paths.js')(app);
require('./socket_events/main.js')(io,pool);