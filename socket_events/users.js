const { default: axios } = require('axios');

module.exports = function (io,pool,socket) {
    let  Mysql =   require('../database_libs/main.js');
    let  shared =   require('./shared_vars');
    Mysql = new Mysql(pool);
    // let users_list = [];
    // add new driver in drivers opject
    socket.on('ping',(data)=>{
        // console.log('ping is recived');
    });
    
    socket.on('connect user',async (data)=>{
        console.log('userConnected '+JSON.stringify(data));
        // get this driver user data from Database
        try { 
            let UserData = await Mysql.getUserData(data.userId);
            let userTrips = await Mysql.getUserTrips(data.userId);
            let dataOfTheUser = UserData[0];
            dataOfTheUser.long = data.long;
            dataOfTheUser.lat = data.lat;
            dataOfTheUser.socket_id = socket.id;
            shared.PushIfNew(dataOfTheUser,'users_list');
            io.emit('update users pins',shared.getUserList());
            socket.emit('update user trips',userTrips);
        } catch (error) {
            console.log(error);
        }
    })
    socket.on('init booking',async (data)=>{

    })
    socket.on('accept trip request',(data)=>{
        console.log('user side')
        console.log(data)
        console.log( shared.getUserList() );
        

        let driveObject = shared.getDriverBySocketId(shared.getActiveDriver() ); 
        let userObject = shared.getUserByUserId(data.user_id);
        shared.attachWatcher(socket.id, userObject)
        axios.get('http://over.macber-eg.com/general/tripDetails?status=taken&trip_id='+data.id+'&driver_id='+driveObject.id).then(function (response) {
            console.log(userObject);
            io.to(userObject.socket_id).emit('watch driver', response.data)
            console.log("userObject socket id " + userObject.socket_id);
        })
        .catch(function (error) {
            console.log(error);
        }) 
        .then(function () {
            // always executed
        });

        console.log(shared.getDriverList())
    })

    socket.on('user canceled trip request',(data)=>{
        console.log('cancellllllllllllllllll')
        console.log(data)
        console.log(data.trip_id)
        axios.get('http://over.macber-eg.com/general/changeTripStatus/caneclByuser?trip_id='+data.trip_id).then(function (response) {
            io.emit('user canceled trip request', response.data)
            console.log(response.data);
        })
        .catch(function (error) {
            console.log(error);
        }) 
        .then(function () {
            // always executed
        });
        io.emit('hideDialogRequest')
        console.log(shared.getDriverList())
    })

    socket.on('driver arrived',(data)=>{
        console.log(data)
        if(data.id === undefined){
            data.id = data;
        }
        axios.get('http://over.macber-eg.com/general/changeTripStatus/arrival?trip_id='+data.id).then(function (response) {
            console.log(response.data);
            io.emit('driver arrived', response.data)
        })
        .catch(function (error) {
            console.log(error);
        }) 
        .then(function () {
            // always executed
        });
    })

    socket.on('driver start trip',(data)=>{
        if(data.id === undefined){
            data.id = data;
        }
    
        axios.get('http://over.macber-eg.com/general/changeTripStatus/trip_started?trip_id='+data.id).then(function (response) {
            console.log(response.data);
            io.emit('driver start trip', response.data)
        })
        .catch(function (error) {
            console.log(error);
        }) 
        .then(function () {
            // always executed
        });
    })

    socket.on('driver collect money',(data)=>{
        axios.get('http://over.macber-eg.com/general/changeTripStatus/collected?trip_id='+data).then(function (response) {
            console.log(response.data);
            io.emit('driver collect trip', response.data)
        })
        .catch(function (error) {
            console.log(error);
        }) 
        .then(function () {
            // always executed
        });
    })

    let PushIfNewUser = ($data)=>{ 
        // loop through the target  array
        let $targetIndex;
        let current_users_list = shared.getUserList();
        current_users_list.forEach((element , index)=>{
            if(element.id == $data.id){
                console.log('record found');
                $targetIndex = index;
            }
        })
        if($targetIndex === undefined){
            current_users_list.push($data);
            shared.setUserList(current_users_list);
            return $data;
        }else{
            return current_users_list[$targetIndex];
        }
    }
};