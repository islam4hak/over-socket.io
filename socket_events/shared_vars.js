// counter.js
let value = 0
let name = "";
let driver_list = [];
let users_list = [];
let active_driver="";
module.exports = {
    increment: () => value++,
    get: () => value,
    setName: ($scopedName) => name = $scopedName,
    getName: () => name,
    setActiveDriver: ($scopedName) => active_driver = $scopedName,
    getActiveDriver: () => active_driver,
    setDriversList: ($pass) => driver_list = $pass,
    getDriverList: () => driver_list,
    getFreeDrivers: ()=>{
        let free = [];
        driver_list.forEach(function (element){
            if(element.watcher === undefined) {
                free.push(element)
            }
        });
        return free
    },
    setUserList: ($pass) => users_list = $pass,
    getUserList: () => users_list,
    getDriverBySocketId:($socket_id) =>{
        let $returnDriverIndex;
        driver_list.forEach((element , index)=>{
        if(element.socket_id == $socket_id){
            // console.log('record found - Driver Socket ');
            $returnDriverIndex = index; 
        }
        })
        return driver_list[$returnDriverIndex];

    },
    getDriverByUserId:(UserId) =>{
        let $returnDriverIndex;
        driver_list.forEach((element , index)=>{
        if(element.id == UserId){
            console.log('record found - Driver id ');
            $returnDriverIndex = index; 
        }
        })
        return driver_list[$returnDriverIndex];

    },
    attachWatcher:($socket_id, user) =>{
    let $returnDriverIndex;
    console.log($socket_id)
    driver_list.forEach((element , index)=>{
        if(element.socket_id == $socket_id){
            console.log('record found - watcher ');
            $returnDriverIndex = index; 
        }
    })
    driver_list[$returnDriverIndex].watcher = user;
},
    getUserBySocketId:($socket_id) =>{
        let $returnDriverIndex;
        users_list.forEach((element , index)=>{
        if(element.socket_id == $socket_id){
            console.log('record found - User Socket');
            $returnDriverIndex = index; 
        }
        })
        return users_list[$returnDriverIndex];
    
    },
    getUserByUserId:($userId) =>{
        let $returnDriverIndex;
        users_list.forEach((element , index)=>{
        if(element.id == $userId){
            console.log('record found - User');
            $returnDriverIndex = index; 
        }
        })
        return users_list[$returnDriverIndex];
    
    },
    PushIfNew:  ($data,varName)=>{
        //TODO we need to update socket.id  if the user found 
        let loadedList = varName == "drivers_list"? driver_list :users_list;
        let $targetIndex;
        let current_list = loadedList;
        current_list.forEach((element , index)=>{
            if(element.id == $data.id){
                console.log('record found - Push');
                $targetIndex = index;
            }
        })
        if($targetIndex === undefined){
            console.log('push is now creating');
            current_list.push($data);
            varName == "drivers_list"? driver_list=current_list :users_list= current_list;
            return $data;
        }else{
            //update the  current list 
            console.log('push is now updating ');
            current_list[$targetIndex] = $data;
            varName == "drivers_list"? driver_list=current_list :users_list= current_list;
            return current_list[$targetIndex];
        }
    }
}