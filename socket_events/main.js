const { default: axios } = require('axios');

module.exports = function (io,pool) {
    let  Mysql =   require('../database_libs/main.js');
    Mysql = new Mysql(pool);
    const  shared =   require('./shared_vars.js');

    // const drivers_list = [];
    // let users_list = [];
    let current_requestes = [];
    io.on('connection' , async  (socket) =>{
        console.log("user connected");
        // socket.emit('get my id',{});
        try {
            const driver_result  = await Mysql.GetAllDrivers();
            const users_result = await Mysql.GetAllUsers();
            socket.emit("update driver list", driver_result);
            socket.emit("update users list", users_result);
        } catch (error) {
            console.log(error);
        }

        // add new user in users object 
        socket.on('connect driver',async (data)=>{
            console.log('Driver Connected '+JSON.stringify(data));
            // get this driver user data from Database 
            try {
                let DriverData = await Mysql.getDriverData(data.driverId)
                let dataOfTheDriver = DriverData[0];
                dataOfTheDriver.long = data.long;
                dataOfTheDriver.lat = data.lat;
                dataOfTheDriver.socket_id = socket.id;
                shared.PushIfNew(dataOfTheDriver,"drivers_list");
                io.emit('update drivers pins',shared.getDriverList());
            } catch (error) {
                console.log(error);
            }
        })

        // booking 
        socket.on('booking',async (data)=>{
            // check if there is any active drivers to loop through
            console.log("dataaaaaaaaaaaaaa " )
            console.log(data)
            let current_driver_list = shared.getFreeDrivers();
            if(current_driver_list.length == 0){
                socket.emit('message' , {msg:"Error no driver is here to call ",code:1301});
            }else{
                // loop through the drivers
                try {
                    const tripDetails = await Mysql.getTripResult(data.tripId);
                    // console.log(tripDetails);
                    current_driver_list.forEach((element, index)=>{
                        let $reciverId = element.socket_id;
                        io.to($reciverId).emit('recive request', tripDetails[0]);
                        // io.clients[$reciverId].send('recive trip request',(tripDetails)=>{
                        //     // socket.emit('cancel current request',({requestID:data.requestId}));
                        // })
                    });
                } catch (error) {
                    console.log(error);
                }
            }
        })

        socket.on('accept trip request',(data)=>{
            console.log('driver side')
            shared.setActiveDriver(socket.id);
            io.emit('hideDialogRequest',data)
            // send the client the accepted driver 
            // $reciverId = data.clientId;
            // io.clients[$reciverId].send('driver approved',(data)=>{
            //    socket.emit('cancel current request',({requestID:data.requestId}));
            // })
            //cancel all the current request from all the other drivers 
        })

        socket.on('driver canceled trip request',(data)=>{
            console.log('cancel from driver side')
            console.log(data)
            axios.get('http://over.macber-eg.com/general/changeTripStatus/changing?trip_id='+data.id).then(function (response) {
                io.emit('driver canceled trip request', response.data)
                console.log(response.data);
            })
            .catch(function (error) {
                console.log(error);
            }) 
            .then(function () {
                // always executed
            });
    
            console.log(shared.getDriverList())
        })

        socket.on('driver update my location',(data)=>{
            // console.log(data)
            let current_driver_list = shared.getDriverList();
            current_driver_list.forEach((element , index)=>{
                if(element.socket_id == socket.id){
                    // console.log('record found');
                    current_driver_list[index].lat = parseFloat(data.lat)
                    current_driver_list[index].long =parseFloat(data.lng)
                }
            })
            shared.setDriversList(current_driver_list);
            io.emit('update drivers pins',{data:current_driver_list});
            let currendDriver = shared.getDriverBySocketId(socket.id)
            if (currendDriver !== undefined) {
                if (currendDriver.watcher !== undefined) {
                    let watcher_id = currendDriver.watcher.socket_id;
                    io.to(watcher_id).emit("update my driver location", {data:currendDriver});
                }
            }
        })

        socket.on('driver finish trip',(data)=>{
            axios.get('http://over.macber-eg.com/general/changeTripStatus/finish?trip_id='+data).then(function (response) {
                console.log(response.data);
                io.emit('driver finish trip', response.data)
            })
            .catch(function (error) {
                console.log(error);
            }) 
            .then(function () {
                // always executed
            });
        })
        require('./users.js')(io,pool,socket);
    })
}